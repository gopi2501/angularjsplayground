//<reference path="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js" />

var myApp = angular
                .module('myApp', [])
                .controller('myController', myController);


// var myController = function($scope) {
function myController($scope) {
    $scope.message = "Angular JS Tutorial";
    $scope.employee = {
        firstName: "David",
        lastName: "Hastings",
        gender: "Male"
    }
    //below is for ng-src
    $scope.country = {
        name: 'Hello',
        source: 'google',
        path: './hello.jpeg'
    }
    //two-way binding and ng-model
    $scope.employeeForDataBinding = {
        firstName: "David",
        lastName: "Hastings",
        gender: "Male"
    }

    //ng-repeat
    $scope.employeesForNgRepeat = [
            { firstName: "David", lastName: "Hastings", gender: "Male", salary: '4500' },
            { firstName: "Harish", lastName: "Pochampally", gender: "Male", salary: '4500' },
            { firstName: "Leela", lastName: "Mahajan", gender: "Female", salary: '24500' },
        ]
    $scope.countries = [
        { name: 'UK', cities: [
            {name: 'London'},
            {name: 'Manchester'},
            {name: 'Brimingham'},
        ]
        },
        { name: 'USA', cities: [
            {name: 'Los Angeles'},
            {name: 'Chicago'},
            {name: 'Huston'},
        ]
        }
    ];
}
