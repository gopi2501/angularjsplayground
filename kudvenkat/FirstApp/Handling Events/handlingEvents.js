var myApp = angular
                .module('myApp', [])
                .controller('handlingEventsController', handlingEventsController);


// var myController = function($scope) {
function handlingEventsController($scope) {
    var technologies = [
        { name: "C#", likes: 0, dislikes: 0},
        { name: "ASP.NET", likes: 0, dislikes: 0},
        { name: "ANGULAR JS", likes: 0, dislikes: 0},
        { name: "ANGULAR TS", likes: 0, dislikes: 0},
        { name: "SQL SERVER", likes: 0, dislikes: 0},
    ];
    $scope.technologies= technologies;
    $scope.incrementLikes = function(technology){
        technology.likes++;
    }
    $scope.incrementDisLikes = function(technology){
        technology.dislikes++;
    }
}