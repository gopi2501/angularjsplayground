var myApp = angular
                .module('myApp', [])
                .filter('genderfilter', genderFunction)
                .controller('filterController', filterController);


// var myController = function($scope) {
function filterController($scope) {

    $scope.employees = [
        { name: 'Ben', dateOfBirth: new Date("November 23, 1980"), gender: "Male", salary: 55000.78 },
        { name: 'Sara', dateOfBirth: new Date("May 05, 1970"), gender: "Female", salary: 68000 },
        { name: 'Mark', dateOfBirth: new Date("August 29, 1987"), gender: "Male", salary: 57000 },
        { name: 'Pam', dateOfBirth: new Date("January 9, 1971"), gender: "Female", salary: 53000 },
        { name: 'Todd', dateOfBirth: new Date("March 5, 1979"), gender: "Male", salary: 60000 },
    ];
    $scope.rowLimit = 3;
    $scope.sortColumn = 'name';
    $scope.reverseSort = false;
    $scope.sortData = function(column){
        $scope.reverseSort = ($scope.sortColumn == column)? !$scope.reverseSort: false;
        $scope.sortColumn = column;
    }
    $scope.getSortClass = function(column){
        if($scope.sortColumn == column){
            return $scope.reverseSort ? 'arrow-down': 'arrow-up';
        }
        return '';
    }

    $scope.customEmployees = [
        {  name: 'Ben', gender: 1, salary: 555000 },
        {  name: 'Mithili', gender: 2, salary: 6000 },
        {  name: 'Flintoff', gender: 1, salary: 13000 },
        {  name: 'Stacy', gender: 2, salary: 8000 },
        {  name: 'Andrew', gender: 1, salary: 9700 },
        {  name: 'Some Not Disclosed', gender: 3, salary: 9700 },
    ];
}

/** Custom filter should return a function */

function genderFunction() {
    return function genderCustomFilter(gender){
        switch(gender) {
            case 1: return 'Male';
            case 2: return 'Female';
            default: return 'Not Disclosed';
        }
    }
}