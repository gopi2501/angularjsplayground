var myApp = angular
                .module('myApp', [])
                .controller('NgShowAndNgHide', NgShowAndNgHide);


// var myController = function($scope) {
function NgShowAndNgHide($scope) {
    $scope.employees = [
        { name: 'Ben', city: 'Delhi', gender: "Male", salary: 55000.78 },
        { name: 'Sara', city: 'Mumbai', gender: "Female", salary: 68000 },
        { name: 'Mark', city: 'Ghaziabad', gender: "Male", salary: 57000 },
        { name: 'Pam', city: '', gender: "Female", salary: 53000 },
        { name: 'Todd', city: 'Hyd', gender: "Male", salary: 60000 },
    ];
}